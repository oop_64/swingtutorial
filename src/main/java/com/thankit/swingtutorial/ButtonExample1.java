package com.thankit.swingtutorial;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class ButtonExample1 extends JFrame {
    JButton button;
    JTextField text;
    JButton clearbutton;
    public ButtonExample1() {
        super("First JFrame");
        text = new JTextField();
        text.setBounds(50, 50, 150, 20);
        button = new JButton("Welcome");
        button.setBounds(50, 100, 110, 30);
        button.setIcon(new ImageIcon("welcome.png"));
        button.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Click Button");
                text.setText("Welcome to Burapha");
            }

        });
        clearbutton = new JButton("Clear");
        clearbutton.setBounds(165, 100, 95, 30);
        clearbutton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
               text.setText("");
                
            }
            
        });
        clearbutton.setIcon(new ImageIcon("delete.png"));
        this.add(clearbutton);
        this.add(text);
        this.add(button);
        this.setLayout(null);
        this.setSize(400, 400);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public static void main(String[] args) {
        ButtonExample1 frame = new ButtonExample1();
    }
}
