package com.thankit.swingtutorial;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class QueueApp extends JFrame {
    JTextField txtName;
    JLabel lblQueueLsit, lblCurrent;
    JButton btnAddQButton, btnGetQueue, btnClearQueue;
    LinkedList<String> queue;

    public QueueApp() {
        super("Queue App");
        queue = new LinkedList();
        this.setSize(400, 300);
        this.setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);

        txtName = new JTextField();
        txtName.setBounds(30, 10, 200, 20);
        txtName.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                addQueue();
                
            }
            
        });

        btnAddQButton = new JButton("Add Queue");
        btnAddQButton.setBounds(250, 10, 100, 20);
        btnAddQButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                addQueue();
                
            }
            
        });

        btnGetQueue = new JButton("Get Queue");
        btnGetQueue.setBounds(250, 40, 100, 20);
        btnGetQueue.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                getQueue();
                
            }});

        btnClearQueue = new JButton("Clear Queue");
        btnClearQueue.setBounds(250, 70, 100, 20);
        btnClearQueue.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                clearQueue();
                
            }});

        lblQueueLsit = new JLabel("Empty");
        lblQueueLsit.setBounds(30, 40, 200, 20);

        lblCurrent = new JLabel("?");
        lblCurrent.setBounds(30, 70, 200, 50);
        lblCurrent.setHorizontalAlignment(JLabel.CENTER);
        lblCurrent.setFont(new Font("Serif", Font.PLAIN, 50));

        showQueue();

        this.add(lblCurrent);
        this.add(lblQueueLsit);
        this.add(btnClearQueue);
        this.add(btnGetQueue);
        this.add(txtName);
        this.add(btnAddQButton);

    }

    public void showQueue() {
        if (queue.isEmpty()) {
            lblQueueLsit.setText("Empty");
        } else {
            lblQueueLsit.setText(queue.toString());
        }

    }
    public void getQueue(){
        if(queue.isEmpty()) {
            lblCurrent.setText("?");
            return;
        }
        String name = queue.remove();
        lblCurrent.setText(name);
        showQueue();
    }

    public void addQueue() {
        String name = txtName.getText();
        if (name.equals("")) {
            return;
        }
        queue.add(name);
        txtName.setText("");
        showQueue();
    }
    public void clearQueue(){
        queue.clear();
        lblCurrent.setText("?");
        txtName.setText("");
        showQueue();
    }

    public static void main(String[] args) {
        QueueApp app = new QueueApp();
    }
}
