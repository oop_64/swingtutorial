package com.thankit.swingtutorial;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class HelloMyName extends JFrame {
    JLabel lbName;
    JTextField txtName;
    JButton btnHello;
    JLabel lbHello;
    public HelloMyName(){
        super("Hello My Name");
        lbName = new JLabel("Name: ");
        lbName.setBounds(10, 10, 50, 20);
        lbName.setHorizontalAlignment(JLabel.RIGHT);
        txtName = new JTextField();
        txtName.setBounds(70, 10, 200, 20);

        btnHello = new JButton("Hello");
        btnHello.setBounds(10, 40, 250, 20);
        btnHello.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                String myName = txtName.getText();
                lbHello.setText("Hello " + myName);
                
            }});
        
        lbHello = new JLabel("Hello My name");
        lbHello.setHorizontalAlignment(JLabel.CENTER);
        lbHello.setBounds(30, 70, 250, 20);
        this.add(lbHello);
        this.add(btnHello);
        this.add(txtName);
        this.add(lbName);
        this.setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        this.setSize(400,300);
    }
    public static void main(String[] args) {
        HelloMyName frame = new HelloMyName();
    }
}
